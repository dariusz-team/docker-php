FROM php:7.2.1-fpm-alpine3.7


RUN \
    echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk --update add \
      	php7-mongodb \
        php7-curl \
        php7-fpm \
        php7-gd \
        php7-intl \
        php7-json \
        php7-mbstring \
        php7-opcache \
        php7-pdo \
        php7-posix \
        php7-session \
        php7-xml \
        php7-ctype \
        php7-openssl \
        openssh-client \
        rsync \
    && rm -rf /var/cache/apk/*

RUN apk add --no-cache \
		$PHPIZE_DEPS \
		openssl-dev

RUN pecl install mongodb
RUN docker-php-ext-enable mongodb


COPY php.ini /etc/php7/conf.d/50-setting.ini
COPY php-fpm.conf /etc/php7/php-fpm.conf

VOLUME /app
EXPOSE 9000

CMD ["php-fpm7", "-F"]